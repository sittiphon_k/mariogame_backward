package platformer;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import platformer.controller.DrawingLoop;
import platformer.controller.GameLoop;
import platformer.view.Platform;

import java.io.IOException;

public class Main extends Application {
    private static Platform platform;
    private static GameLoop gameLoop;
    private static DrawingLoop drawingLoop;

    static {
        try {
            platform = new Platform();
            gameLoop = new GameLoop(platform);
            drawingLoop = new DrawingLoop(platform);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        platform = new Platform();

        Scene scene = new Scene(platform, platform.WIDTH, platform.HEIGHT);
        scene.setOnKeyPressed(event -> platform.getKeys().add(event.getCode()));
        scene.setOnKeyReleased(event -> platform.getKeys().remove(event.getCode()));

        primaryStage.setTitle("platformer");
        primaryStage.setScene(scene);
        primaryStage.show();

        gameLoop = new GameLoop(platform);
        drawingLoop = new DrawingLoop(platform);

        (new Thread(gameLoop)).start();
        (new Thread(drawingLoop)).start();
    }

    @Override
    public void stop() {
        System.exit(0);
    }

    public static Platform getPlatform() {
        return platform;
    }
}
