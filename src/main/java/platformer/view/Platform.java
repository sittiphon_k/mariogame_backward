package platformer.view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import platformer.model.Character.Character;
import platformer.model.Keys;

import java.io.IOException;

public class Platform extends Pane {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    public static final int GROUND = 300;

    private int money;
    private int time;

    private Image platformImg;
    private ImageView backgroundImg;

    private Character mainCharacter;

    private Keys keys;

    public Platform() throws IOException {
        money = 50;
        time = 30;
        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/img/background/backgroundNormal.png"));
        backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        mainCharacter = new Character(30, 30, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W);

        getChildren().add(backgroundImg);
        getChildren().addAll(mainCharacter);
    }

    public Character getCharacter() {
        return mainCharacter;
    }

    public Keys getKeys() {
        return keys;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}

