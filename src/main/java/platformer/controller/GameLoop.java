package platformer.controller;

import platformer.model.Character.Character;
import platformer.view.Platform;

public class GameLoop implements Runnable {
    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;
    private float refreshRate = 1000.0f / 60;

    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 10;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void update(Character character) {
        if (platform.getKeys().isPressed(character.getLeftKey()) || platform.getKeys().isPressed(character.getRightKey())) {
            character.getImageView().tick();
        }
        if (platform.getKeys().isPressed(character.getLeftKey())) {
            character.setScaleX(-1);
            character.moveLeft();
        }
        if (platform.getKeys().isPressed(character.getRightKey())) {
            character.setScaleX(1);
            character.moveRight();
        }
        if (!platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey())) {
            character.stop();
        }
        if (platform.getKeys().isPressed(character.getUpKey())) {
            character.jump();
        }
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep((long) refreshRate);
            } catch (InterruptedException e) {
            }

            try {
                Thread.sleep((long) (interval - refreshRate));
            } catch (InterruptedException e) {
            }
            update(platform.getCharacter());
        }
    }
}
