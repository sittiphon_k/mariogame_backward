package platformer.controller;

import platformer.model.Character.Character;
import platformer.view.Platform;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void checkDrawCollisions(Character character) throws InterruptedException {
        character.checkReachGameWall();
        character.checkReachHighest();
        character.checkReachFloor();
    }

    private void paint(Character character) {
        character.repaint();
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep((long) interval);
            } catch (InterruptedException e) {
            }

            try {
                checkDrawCollisions(platform.getCharacter());
            } catch (InterruptedException e) {
            }
            paint(platform.getCharacter());

        }
    }
}
